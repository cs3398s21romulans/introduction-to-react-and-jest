import React from 'react';
import Square from './Square.js';
import {calculateWinner} from './calculateWinner.js';

export default class Board extends React.Component {
	// Initialize the Board class by initializing the state of the squares.
    constructor(props) {
    super(props);
    this.state = {
      squares: Array(9).fill(null),
      xIsNext: true,
    };
  }

  //
  // Each time a player moves, xIsNext (a boolean) will be flipped to determine which player goes next and the gameâ€™s state will be saved.
  handleClick(i) {
  	//We use .slice() to create a copy of the squares array to modify instead of modifying the existing array. 
    const squares = this.state.squares.slice();
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
    	squares: squares,
    	xIsNext: !this.state.xIsNext,});
  }
  //
  // Pass in the state (maintained by the Board) into each square, where state is either X,O or null.
  // Also pass a (callback) function that will tell Board the state of the square after a click.
  renderSquare(i) {
    return (
      <Square
        value={this.state.squares[i]}
        onClick={() => this.handleClick(i)}
      />
    );
  }

  render() {
    const winner = calculateWinner(this.state.squares);
    let status;
    if (winner) {
      status = 'Winner: ' + winner;
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }
    //const status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');

    return (
      <div>
        <div className="status">{status}</div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}