import React from 'react';

/*
	1 The onClick prop in the built-in DOM <button> component tells React to set up a click event listener.
	2 When the button is clicked, React will call the onClick event handler that is defined in Square’s render() method.
	3 In this case, the event handler calls this.props.onClick(). The Square’s onClick prop is specified by the Board.
	4 Since the Board passed onClick={() => this.handleClick(i)} to Square, the Square calls this.handleClick(i) when clicked.
*/

export default class Square extends React.Component {
   // The below onClick phrase could alternately be written: <button className="square" onClick={function() {this.props.onClick(); }}> 
  // NOTE:  Adding comments (include those with /* */) sinside a "render" may cause the javascript compilers to fail with odd error messages. 
  // When a Square is clicked, the onClick function provided by the Board is called.
  render() {
    return (
      <button 
      	className="square" 
      	onClick={() => this.props.onClick()}>  
		{this.props.value}
      </button>
    );
  }
}